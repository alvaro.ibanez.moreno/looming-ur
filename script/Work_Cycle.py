#!/usr/bin/python
#https://automaticaddison.com/how-to-move-a-simulated-robot-arm-to-a-goal-using-ros/
import os
import pickle
import rospy
import roslaunch
import actionlib
from Robot_Info import *
from sensor_msgs.msg import JointState
from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectoryPoint

class WorkCycle():

    PythonFilePath=os.path.dirname(__file__)
    RelFilesDirPath='../data/'
    FilesDirectory=os.path.abspath(os.path.join(PythonFilePath, RelFilesDirPath))

    def set_robot_editor_move(self, Robot, Editor, Movement):
        self.Robot=Robot
        self.Editor=Editor
        self.Movement=Movement


    def import_script(self, FileName='cycle.txt'):
        with open(self.FilesDirectory +'/'+ FileName) as f:
            self.script = f.readlines()
        print(str(self.script))


    def load_cycle_data(self, FileName='cycle.pickle'):
        self.Editor.load_file(FileName)
        self.CycleData=self.Editor.DataDict


    def execute_script(self):
        IfExec=[True]
        WhileExec=[True]
        WhileLine=[]
        LineNum=0
        while LineNum<len(self.script):
            line = self.script[LineNum]
            words=line.split()
            print(IfExec)
            print(line)


            if words[0] == 'load' and WhileExec[-1] and IfExec[-1]:
                print('Loading file: ' + words[1])
                self.Editor.reset_data()
                self.Editor.load_file(words[1])
                self.CycleData=self.Editor.DataDict

            if words[0] == 'import' and WhileExec[-1] and IfExec[-1]:
                print('Importing file: ' + words[1])
                self.Editor.reset_data()
                self.Editor.import_data(words[1])
                self.CycleData=self.Editor.DataDict

            if words[0] == 'moveto' and WhileExec[-1] and IfExec[-1]:
                print('Moving to: ' + words[1])
                self.Movement.JS=[]
                self.Movement.JS.append(self.CyclePoints[words[1]].position)
                self.Movement.set_move_duration()
                self.Movement.move_robot()

            if words[0] == 'wait' and WhileExec[-1] and IfExec[-1]:
                if words[1] == 'time':
                    print('waiting '+words[2]+'s')
                    rospy.sleep(float(words[2]))
                if words[1] == 'user':
                    print('Waiting for user input')
                    input()
                if words[1] == 'var':
                    while not(self.CycleData[words[2]]):
                        rospy.sleep(0.25)
                        self.load_cycle_data()

            if words[0] == 'set' and WhileExec[-1] and IfExec[-1]:
                if words[1] == 'user':
                    words.append(input('Set data:'))
                if type(self.CycleData[words[1]]) is str:
                    self.CycleData[words[1]]=words[2]
                if type(self.CycleData[words[1]]) is float:
                    self.CycleData[words[1]]=float(words[2])
                if type(self.CycleData[words[1]]) is bool:
                    self.CycleData[words[1]]=bool('T' in words[2])

            if words[0] == 'while' and WhileExec[-1] and IfExec[-1]:
                if self.CycleData[words[1]]:
                    WhileExec.append(True)
                    WhileLine.append(LineNum)
                else:
                    WhileExec.append(False)

            if words[0] == 'endwhile' and IfExec[-1]:
                if WhileExec[-1]:
                    LineNum=WhileLine[-1] - 1
                    WhileLine.pop()
                else:
                    WhileExec.pop()

            if words[0] == 'if' and WhileExec[-1]:
                if self.CycleData[words[1]] and IfExec[-1]:
                    IfExec.append(True)
                else:
                    IfExec.append(False)

            if words[0] == 'else' and WhileExec[-1]:
                if IfExec[-1]:
                    IfExec[-1]=False
                else:
                    IfExec[-1]=True

            if words[0] == 'endif' and WhileExec[-1]:
                IfExec.pop()

            LineNum=LineNum+1


    def work_cycle_menu(self, I=''):

            E=0
            while E==0:
                if I=='':
                    print('-------------------')
                    print('Work Cycle:')
                    print('1->Load Work Cycle Data')
                    print('2->Load Cycle Points')
                    print('3->Load Script')
                    print('4->Execute Script')

                    print('0->Exit')
                    I=input()

                if I=='1':
                    self.Editor.load_file('cycledata')
                    self.CycleData=self.Editor.DataDict

                elif I=='2':
                    self.Editor.load_file(self.CycleData['PointFile'])
                    self.CyclePoints=self.Editor.DataDict

                elif I=='3':
                    self.import_script(self.CycleData['ScriptFile'])

                elif I=='4':
                    self.execute_script()

                elif I=='0': #Exit
                    E=1
                else:
                    print('Input Error')

                I=''
